#!/bin/bash

NEWLOC=`curl -L -I http://www.shirt-pocket.com/downloads/SuperDuper!.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' 2>/dev/null | grep -i '^Location' | tail -1 | sed 's/[Ll]ocation: //' | tr -d '\r'`

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi