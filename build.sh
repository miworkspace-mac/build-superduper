#!/bin/bash -ex

# CONFIG
prefix="SuperDuper"
suffix=""
munki_package_name="SuperDuper"
display_name="SuperDuper"
icon_name=""
category="Utilities"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

# Build pkginfo
#/usr/local/munki/makepkginfo app.dmg > app.plist

#convert DMG to CDR to bypass EULA
#/usr/bin/hdiutil convert -quiet app.dmg -format UDTO -o app.cdr
#rm -rf app.dmg

## Mount disk image on temp space
#mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.cdr | awk '/private\/tmp/ { print $3 } '`

mountpoint=`yes | hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $3 } '`


mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R $app_in_dmg build-root/Applications

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" $app_in_dmg/Contents/Info.plist`

#creating a components plist to turn off relocation
/usr/bin/pkgbuild --analyze --root build-root/ Component-${munki_package_name}.plist
plutil -replace BundleIsRelocatable       -bool NO Component-${munki_package_name}.plist
plutil -replace BundleHasStrictIdentifier -bool NO Component-${munki_package_name}.plist

#build PKG
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.${munki_package_name} --install-location / --component-plist Component-${munki_package_name}.plist --version ${version} app.pkg

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg ${key_files} | /bin/bash > app.plist

plist=`pwd`/app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' "${plist}"

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.10.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" icon_name "${icon_name}"
defaults write "${plist}" category "${category}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
displayname=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"



# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# delete component plist to prevent upload
rm -rf Component-${munki_package_name}.plist

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
